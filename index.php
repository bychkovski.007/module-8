<?php

class TelegraphText
{

    private $title;
    private $text;
    private $author;
    private $published;
    private $slug;

    public function __construct($author, $slug)
    {
        $this->author = $author;
        $this->slug = $slug . '.txt';
        $this->published = date('r');

    }

    public function storeText()
    {
        $postArray = [
            'title' => $this->title,
            'text' => $this->text,
            'author' => $this->author,
            'published' => $this->published
        ];

        $arr = serialize($postArray);
        file_put_contents($this->slug, $arr);
    }

    public function loadText()
    {
        if (file_exists($this->slug)) {
            $file = file_get_contents($this->slug);
            $file = unserialize($file);

        } else echo 'Файл не найден';
        return ($file);
    }

    public function editText($title, $text)
    {

        $this->title = $title;
        $this->text = $text;

    }
}

$text1 = new TelegraphText('Me', 'text1');

$text1->editText('Title1 changed', 'Text1');

$text1->storeText();


$text2 = new TelegraphText('Me2', 'text2');

$text2->editText('Title2', 'Text2');

$text2->storeText();

print_r($text1->loadText());
print_r($text2->loadText());


